#include <iostream>

void IsEvenOrNot(bool a, int N) 
{
	for (int i = 0; i <= N; i++)
		{
			if (i % 2 == a)
			std::cout << i << " ";
		}
}

int main()
{
	bool a;
	int N;
	
	std::cout << "Enter number:" << "\n";
	std::cin >> N;
	
	std::cout << "Enter 1 to see uneven nubmers or 0 to see only even numbers : " << "\n";
	std::cin >> a;

	IsEvenOrNot(a, N);

	return 0;
}